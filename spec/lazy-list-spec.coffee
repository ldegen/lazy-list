{resolve, delay} = Promise = require "bluebird"
{Readable, Writable} = require "stream"
{
  unit
  bind
  toArray
  wrap
  list
  emptyList
  concat
  flatten
  map
  car
  cdr
  cons
  foldl
  fromReadable
  toReadable
  forEach
  isEmptyNow
  isEmpty
} = require "../src/lazy-list"
ChunkSink = (opts)->
  chunks = []
  output = new Writable opts
  output._write = (chunk, enc, next)->
    if chunk?
      chunks.push chunk
    next()
  output.promise = new Promise (resolve, reject)->
    output.on "error", reject
    output.on "finish", -> resolve chunks
  output

describe "A Lazy List", ->
  it "can be created from an array and a callback for fetching more", ->
    ll = wrap list [1,2,3], -> list [4,5,6], -> list [7,8,9], -> emptyList
    expect(ll.toArray()).to.eventually.eql [1,2,3,4,5,6,7,8,9]

  it "can be created from an array", ->
    expect(toArray list [1,2,3,4]).to.eventually.eql [1,2,3,4]

  it "can also be created from just the more-callback", ->
    ll = list -> list [1,2], ->list [3], -> emptyList
    expect(toArray ll).to.eventually.eql [1,2,3]

  it "can deal with callbacks returning a promise", ->
    ll = wrap list [1,2,3], ->resolve list [4,5,6], -> list [7,8,9], ->emptyList
    expect(ll.toArray()).to.eventually.eql [1,2,3,4,5,6,7,8,9]

  it "never evaluates the callback more than once", ->
    calls = 0
    ll = list ->
      calls++
      list [1,2,3]
    toArray ll
      .then -> toArray ll
      .then -> toArray ll
      .then ->
        expect(calls).to.equal 1

  describe "isEmpty", ->
    it "returns a Promise that resolves to true if a list turns out to be empty", ->
      expect(isEmpty list [], ->delay(20).then -> emptyList).to.eventually.equal true
    it "returns a Promise that resolves to true if a list is already known to be empty", ->
      expect(isEmpty emptyList).to.eventually.equal true
    it "returns a Promise that resolves to false if at least some content of the list is already known", ->
      expect(isEmpty list [1]).to.eventually.equal false
    it "returns a Promise that resolves to false if some content appears in the future", ->
      expect(isEmpty list [], ->delay(29).then ->list [42]).to.eventually.equal false

  describe "cons", ->
    it "constructs a list from a CAR/CDR-Pair", ->
      expect(toArray cons 1, cons 2, cons 3, emptyList).to.eventually.eql [1,2,3]
  describe "car", ->
    it "returns the first element of the list", ->
      expect(car list [1,2,3],->emptyList).to.eventually.eql 1
    it "waits for elements to become available if necessary", ->
      expect(car list [], ->delay(50).then -> list [1,2,3], -> emptyList).to.eventually.eql 1
    it "returns a rejected promise if the list is empty", ->
      expect(car emptyList).to.be.rejected
    it "correctly handles lists that asynchronously turn out to be empty", ->
      expect(car list [], -> delay(20).then list []).to.be.rejectedWith "car applied to empty list"

  describe "cdr", ->
    it "returns a list containing all but the first element of the given list", ->
      ll = list [1,2,3],->emptyList
      expect(toArray cdr ll).to.eventually.eql [2,3]
    it "waits for elements to become available if necessary", ->
      expect(toArray cdr list [], ->delay(50).then -> list [1,2,3], -> emptyList).to.eventually.eql [2,3]
    it "returns a rejected promise if the list is empty", ->
      expect(cdr emptyList).to.be.rejected
    it "correctly handles lists that asynchronously turn out to be empty", ->
      expect(cdr list [], -> delay(20).then list []).to.be.rejectedWith "cdr applied to empty list"


  describe "foldl", ->
    it "left-folds the list", ->
      ll = list [1,2,3], ->delay(50).then -> list [4,5,6], -> emptyList
      expect(foldl ll,0,(a,b)->a+b).to.eventually.eql 21
    it "allows ommiting the initial value if elements and accumulator are of the same type", ->
      ll = list [1,2,3], ->delay(50).then -> list [4,5,6], -> emptyList
      expect(foldl ll,(a,b)->a+b).to.eventually.eql 21
    it "returns the initial value if the list is empty", ->
      expect(foldl emptyList, 42, (a,b)->a+b).to.eventually.eql 42
    it "also works if the list turns out to be empty later on", ->
      expect(foldl (list [], -> delay(20).then emptyList), 42, (a,b)->a+b).to.eventually.eql 42
    it "works a few elements are initial available, but requestin more yields an empty list", ->
      expect(foldl (list [1,2,3], -> delay(20).then emptyList), (a,b)->a+b).to.eventually.eql 6

  describe "toArray", ->
    it "waits until it actually hits the end of the list", ->
      ll = list [1,2,3], ->delay(50).then -> list [4,5,6], -> emptyList
      expect(toArray ll).to.eventually.eql [1,2,3,4,5,6]
    it "yields an empty array if the list is empty", ->
      expect(toArray emptyList).to.eventually.eql []
    it "yields an empty array if the list turns out to be empty later on", ->
      expect(toArray list [], ->delay(20).then list [], -> list []).to.eventually.eql []

  describe "map", ->
    it "works as expected", ->
      ll = wrap list [1,2,3], ->resolve list [4,5,6], -> list [7,8,9], -> emptyList
      ll = ll.map (i)-> -i
      expect(ll.toArray()).to.eventually.eql [-1,-2,-3,-4,-5,-6,-7,-8,-9]
  describe "forEach", ->
    it "works as expected", ->
      values = []
      ll = list [1,2,3], ->delay(50).then -> list [4,5,6], -> emptyList
      forEach ll, (v)->values.push v
        .then ->
          expect(values).to.eql [1,2,3,4,5,6]

  describe "concat", ->
    it "concatenates a finit number of a-priori known (lazy) lists", ->
      a = list [1,2], -> list [3,4], -> resolve list [5,6], -> emptyList
      b = list [1,2], ->resolve list [3,4], -> list [5,6], -> emptyList
      c = list [1,2], ->resolve list [3,4], -> resolve list [5,6], -> emptyList
      ll = wrap concat a,b,c
      expect(ll.toArray()).to.eventually.eql [1,2,3,4,5,6,1,2,3,4,5,6,1,2,3,4,5,6]

  describe "flatten", ->
    it "takes a (lazy) list of (lazy) lists and removes one level of nesting", ->
      a = list [1,2], -> list [3,4], -> resolve list [5,6], -> emptyList
      b = list [1,2], ->resolve list [3,4], -> list [5,6], -> emptyList
      c = list [1,2], ->resolve list [3,4], -> resolve list [5,6], -> emptyList
      l = list [a,b], ->resolve list [c,a], -> list [b], -> resolve list [c], -> emptyList
      ll = wrap flatten l
      expect(ll.toArray()).to.eventually.eql [
        1,2,3,4,5,6
        1,2,3,4,5,6
        1,2,3,4,5,6
        1,2,3,4,5,6
        1,2,3,4,5,6
        1,2,3,4,5,6
      ]
  describe "bind", ->
    it "similar to map, but expects f to produce a lazy list", ->
      ll = wrap list [1,2,3], -> resolve list [4,5,6], -> list [7,8,9], -> emptyList
      ll=ll.bind (x)->list [-x,x]
      expect(ll.toArray()).to.eventually.eql [-1,1,-2,2,-3,3,-4,4,-5,5,-6,6,-7,7,-8,8,-9,9]

    it "is associative", ->
      f = (x)->list [-x, x], -> emptyList
      g = (x)->unit x+2
      a=toArray bind (bind (unit 42), f), g
      b=toArray bind (unit 42), (x)->bind (f x), g
      c=[-40,44]
      expect(Promise.all [a,b,c]).to.be.fulfilled.then ([a,b,c])->
        expect(a).to.eql b
        expect(b).to.eql c
        expect(c).to.eql a

  describe "unit", ->
    it "works as neutral element for bind", ->
      f = (x)->
        list [-x, x], -> emptyList
      a=toArray bind (unit 42), f
      b=toArray bind (f 42), unit
      c=toArray f 42
      expect(Promise.all [a,b,c]).to.be.fulfilled.then ([a,b,c])->
        expect(a).to.eql b
        expect(b).to.eql c
        expect(c).to.eql a

  describe "fromReadable", ->
    createInput = (blocks, objectMode=true)->
      r = new Readable
        objectMode: objectMode
        read: (size)->
          @readCalls ?= 0
          @readCalls++
          pushMore = true
          if blocks.length is 0
            #delay(50).then=> @push null
            @push null
          else
            chunks = blocks.shift()
            delay(50).then =>
              while chunks.length > 0 and pushMore
                pushMore = @push chunks.shift()

    it "creates a list of all chunks from a readable stream", ->
      r = createInput [[1,2,3],[4,5]]
      expect(toArray fromReadable r).to.eventually.eql [1,2,3,4,5]
    it "works with raw (non-object-mode) streams", ->
      r = createInput [["yes","no","maybe"],["i am","certain"]], false
      expect(toArray fromReadable r).to.eventually.eql ["yesnomaybe","i amcertain"].map Buffer.from
    it "can be read multiple times", ->
      r = createInput [[1,2,3],[4,5]]
      ll = fromReadable r
      toArray ll
        .then ->
          expect(toArray ll).to.eventually.eql [1,2,3,4,5]

  describe "toReadable", ->
    it "creates a readable stream from a lazy list", ->
      sink = ChunkSink objectMode: true
      toReadable list [1,2,3], ->delay(20).then ->list [5,6], ->delay(20).then -> emptyList
        .pipe sink
      expect(sink.promise).to.eventually.eql [1,2,3,5,6]
    it "can create raw (non-object) streams as well", ->
      sink = ChunkSink objectMode: false
      ll = list ["abc","d","efg"], ->delay(20).then ->list [], ->delay(20).then -> list ["hij","klmno"]
      toReadable ll, objectMode:false
        .pipe sink
      expect(sink.promise).to.eventually.eql ["abc","d","efg","hij","klmno"].map Buffer.from
