Promise = require "bluebird"
{Readable, Transform} = require "stream"
resolveArgs = (f)->(args...)-> Promise.all(args).spread f
empty = [[]]
emptyP = Promise.resolve empty
emptyF = -> emptyP
isEmptyNow = (list)->
  return true if not list?
  return true if list is empty
  try
    [c, m] = list
  catch e
    console.log "list", list
    throw e
  if c.length == 0
    return true if not m?
    return true if m is emptyF
isEmpty = resolveArgs (list)->
  if isEmptyNow list
    return true
  else if chunk(list).length > 0
    return false
  else
    more = moreOf list, isEmpty
    more()

chunk = ([c])-> c

moreOf = (l, f)->
  getMore =([_,more])->
    tailP = more?()
    if tailP then Promise.resolve tailP else emptyP

  if f?
    ->getMore(l).then f
  else
    ->getMore(l)
cons = (x, [c,f])->[[x,c...], f]

car = resolveArgs (list)->
  if isEmptyNow list
    throw new Error("car applied to empty list")
  else
    c = chunk list
    return Promise.resolve c[0] if c.length > 0
    more = moreOf list, (tail)->car tail
    more()

cdr = resolveArgs (list)->
  if isEmptyNow list
    throw new Error("cdr applied to empty list")
  else
    [c,f]=list
    if c.length > 0
      return mklist (c.slice 1),f
    more = moreOf list, (tail)->
      cdr tail
    more()

_foldl = resolveArgs (list, v0,f)->
  return v0 if isEmptyNow list
  c = chunk list
  v = c.reduce f, v0
  more = moreOf list, (tail)->_foldl tail, v, f
  more()

foldl = resolveArgs (list, v0, f)->
  if typeof v0 is "function" and not f?
    car(list).then (v)->_foldl (cdr list), v, v0
  else
    _foldl list, v0, f



unit = (d) ->
  mklist [d]

map = (list,f)->
  return empty if isEmptyNow list
  more = moreOf list, (tail)-> map tail,f
  c = (chunk list).map f
  [c,more]


concat = (a,bs...)->
  if not a?
    return []
  if bs.length == 0
    return a
  if isEmptyNow a
    return concat bs...
  mklist (chunk a), moreOf a, (tail)-> concat tail, bs...

memoize = (f0)->
  return f0 if f0.__ll_memoized

  v = undefined
  called = false
  f = -> if called then v else
    called=true
    v=f0.call this
  f.__ll_memoized=true
  f

mklist = (c,f)->
  if Array.isArray(c)
    [c,memoize f ? emptyF]
  else if typeof c is "function" and not f?
    [[],memoize c]
  else
    throw new Error("cannot create a list from #{c} and #{f}")

flatten = (listOfLists)-> #
  if isEmptyNow listOfLists
    return empty
  flatChunk = concat (chunk listOfLists)...
  mklist (chunk flatChunk), moreOf flatChunk, (chunkTail)->
    more = moreOf listOfLists, (tailOfLists)->
      concat chunkTail, flatten tailOfLists
    more()

bind = (list,f) -> flatten map list, f

toArray = resolveArgs (list)->
  if isEmptyNow list
    return []
  more = moreOf list, (tail)->toArray tail
  Promise
    .all [(chunk list),more()]
    .then ([a,b])->
      a.concat b

forEach = (list, f)->
  unless isEmptyNow list
    chunk list
      .forEach f
    more = moreOf list, (tail)->forEach tail, f
    more()
wrap = (list)->
  bind: (f) -> wrap bind list, f
  map: (f) -> wrap map list, f
  forEach: (f)-> wrap map list, f
  flatten: -> wrap flatten list
  toArray: -> toArray list

fromReadable = (readable, objectMode)->mklist [], ->
  tf = new Transform
    objectMode:objectMode ? readable?._readableState?.objectMode ? true
    transform:(chunk,enc,done)->
      @push chunk
      done()
    flush: (done)->
      @__ll_ended = true
      done()
  fromReadableRecursive readable.pipe(tf)
fromReadableRecursive = (readable)->
  #console.log "enter fromReadable", readable._readableState.ended
  new Promise (resolve, reject)->
    errListener = (e)->
      readable.off "end", endListener
      readable.off "readable", readListener
      #console.log "caught an error", e
      reject e
    endListener = ->
      readable.off "error", errListener
      readable.off "readable", readListener
      #console.log "end was fired"
      #console.log "pending"
      resolve empty
    readListener = ->
      readable.off "error", errListener
      readable.off "end", endListener
      #console.log "readable was fired"
      prefix =[]
      readAtLeastOneChunk = false

      while null isnt (readChunk = readable.read())
        readAtLeastOneChunk = true
        prefix.push readChunk

      #console.log "read",prefix.length, "items"
      ended = readable.__ll_ended
      #console.log "ended", ended
      unless readAtLeastOneChunk
        #console.log "nothing read -> ending"
        resolve empty
      else if ended
        #console.log "read some, but stream has ended -> ending"
        resolve mklist prefix, -> empty
      else
        #console.log "read some, stream hasn't ended -> waiting for more"
        resolve mklist prefix, -> fromReadableRecursive readable

    readable.once "end", endListener
    readable.once "error", errListener
    readable.once "readable", readListener
readChunks = (readable, ll)->
  Promise.resolve ll
    .then (list)=>
      if isEmptyNow list
        readable.push null
        # also return an empty list. It may not strictly be necessary
        # but you never know.
        empty
      else
        # TODO: this is for object mode only
        keepGoing = true
        i=0
        buf = chunk list
        more = moreOf list
        while i < buf.length and keepGoing
          keepGoing = readable.push buf[i]
          i++
        if keepGoing
          # chunk is exhausted, fetch more and recurse.
          readChunks readable, more()
        else
          # there may be unprocessed elements in chunk
          # but downstream asked us to stop pushing for now.
          # We construct a residual list and return it
          # so it can be used for the next call to read.
          mkList buf.slice(i), more



toReadable = (list, opts = {objectMode: true})->
  r = new Readable opts
  r._read = ->
      # note that we DO WANT to overwrite the local variable
      # list in this case. We want to avoid keeping a reference
      # to the initial head of the list in the closure, since this
      # would keep the whole list from being garbage collected.
      list = readChunks this, list
  r

    
module.exports =
  toArray: toArray
  unit:unit
  bind:bind
  map:map
  wrap:wrap
  list:mklist
  cons:cons
  car: car
  cdr: cdr
  emptyList:empty
  concat:concat
  flatten:flatten
  foldl: foldl
  forEach: forEach
  fromReadable: fromReadable,
  toReadable: toReadable,
  isEmptyNow: isEmptyNow
  isEmpty:isEmpty


